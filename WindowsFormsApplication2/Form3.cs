﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form3 : Form
    {
        Conexion strConexion;
        MySqlConnection conn;
        public Form3()
        {
            InitializeComponent();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            strConexion = new Conexion();
            conn = strConexion.conexion();

            fillComboBox();
        }


        public void fillComboBox()
        {

            using (conn)
            {
                string query = "SELECT nombre FROM empresas";

                MySqlCommand cmd = new MySqlCommand(query, conn);

                MySqlDataAdapter da1 = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da1.Fill(dt);

                comboBox1.ValueMember = "nombre";
                comboBox1.DisplayMember = "nombre";
                comboBox1.DataSource = dt;

            }
        }
        public void FillGridServicios()
        {

            MySqlCommand cmd = conn.CreateCommand();

            try
            {
                cmd.CommandText = "select servicios.nombre,poblaciones.localidad,poblaciones.municipio from solicitud "+ 
"inner join serviciosolicitud on serviciosolicitud.idsolicitud = solicitud.idSol "+
"inner join serviciosempresapoblacion on serviciosempresapoblacion.idSep = serviciosolicitud.servicio "+
"inner join servicios on servicios.idSer = serviciosempresapoblacion.servicio "+
"inner join empresaspoblaciones on empresaspoblaciones.EmpresaPoblacion = serviciosempresapoblacion.EmpresaPoblacion "+
"inner join poblaciones on poblaciones.idPob = empresaspoblaciones.poblacion "+
"where empresaspoblaciones.empresa = '" + comboBox1.Text + "' AND MONTH(fecha_solicitud) = MONTH(ADDDATE(CURDATE(), INTERVAL 0 MONTH))";
                conn.Open();
                cmd.ExecuteNonQuery();
                MySqlDataAdapter dm = new MySqlDataAdapter(cmd);
                DataTable ls = new DataTable();
                dm.Fill(ls);

                dataGridView1.DataSource = ls;
                conn.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show(ex.ToString());

            }

        }
        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm = new Form4();
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FillGridServicios();
        }
    }
}

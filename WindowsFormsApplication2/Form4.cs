﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/*
 Autor: Maicky y Deya
 Fecha: 08/03/2017
 Clase: Clase que contiene un crud de las empresas 
     */
namespace WindowsFormsApplication2
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
            pictureBox1.Image = Image.FromFile("enterprise.png");
            pictureBox2.Image = Image.FromFile("details.png");
            pictureBox3.Image = Image.FromFile("services.png");
        }

        /*Abre el administrador de empresas*/
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 frm = new Form1();
            frm.Show();

        }
        /*Abre la intefaz detalles empresas*/
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frm = new Form2();
            frm.Show();
        }
        /*Abre  la intefaz de servicios*/
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 frm = new Form3();
            frm.Show();

        }
    }
}

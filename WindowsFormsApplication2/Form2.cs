﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/*Autor: omar catzin
  Fecha: 14-03-17
     
     */
namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        Conexion strConexion;
        MySqlConnection conn;
     
     
        public Form2()
        {



            InitializeComponent();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            strConexion = new Conexion();
            conn = strConexion.conexion();
          

            fillComboBox();
           

        }

        public void fillComboBox()
        {

            using (conn)
            {
                string query = "SELECT nombre FROM empresas";

                MySqlCommand cmd = new MySqlCommand(query, conn);

                MySqlDataAdapter da1 = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da1.Fill(dt);

                comboBox1.ValueMember = "nombre";
                comboBox1.DisplayMember = "nombre";
                comboBox1.DataSource = dt;

            }
        }
        public void FillGridPerfil()
        {
            
            MySqlCommand cmd = conn.CreateCommand();

            try
            {
                cmd.CommandText = "select nombre,razon_social,direccion,telefono,celular from empresas where nombre ='"+comboBox1.Text+"'";
                conn.Open();
                cmd.ExecuteNonQuery();
                MySqlDataAdapter dm = new MySqlDataAdapter(cmd);
                DataTable ls = new DataTable();
                dm.Fill(ls);

                dataGridView1.DataSource = ls;
                conn.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show(ex.ToString());

            }

        }
        public void FillGridServicios()
        {

            MySqlCommand cmd = conn.CreateCommand();

            try
            {
                cmd.CommandText = "select servicios.nombre as servicio,serviciosempresapoblacion.precio,transporte.nombre as transporte,transporte.plus,poblaciones.localidad,poblaciones.municipio from empresas "+
"inner join empresaspoblaciones on empresaspoblaciones.empresa = empresas.nombre "+
"inner join poblaciones on poblaciones.idPob = empresaspoblaciones.poblacion "+
"inner join serviciosempresapoblacion on serviciosempresapoblacion.EmpresaPoblacion = empresaspoblaciones.EmpresaPoblacion "+
"inner join servicios on servicios.idSer = serviciosempresapoblacion.servicio "+
"inner join transporte on transporte.idTran = servicios.transporte where empresas.nombre ='" + comboBox1.Text + "'";
                conn.Open();
                cmd.ExecuteNonQuery();
                MySqlDataAdapter dm = new MySqlDataAdapter(cmd);
                DataTable ls = new DataTable();
                dm.Fill(ls);

                dataGridView2.DataSource = ls;
                conn.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show(ex.ToString());

            }

        }
        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(comboBox1.Text);
            FillGridPerfil();
            dataGridView1.Enabled = false;
            FillGridServicios();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm = new Form4();
            frm.Show();

        }
    }
}

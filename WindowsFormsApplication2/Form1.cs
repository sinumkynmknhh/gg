﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/*
 Autor: Maicky y Deya
 Fecha: 08/03/2017
 Clase: Clase que contiene un crud de las empresas 
     */
namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            
            InitializeComponent();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void limpiarFormulario()
        {
            tfNom.Text = "";
            tfRaz.Text = "";
            tfDir.Text = "";
            tfTel.Text = "";
            tfCel.Text = "";
        }

        /*Metodo que llena el gridView*/
        private void llernarGrid(DataGridView grid)
        {
            Conexion strConexion = new Conexion();
            MySqlConnection conn = strConexion.conexion();
            MySqlCommand cmd = conn.CreateCommand();

            try
            {
                cmd.CommandText = "SELECT nombre, razon_social, direccion, telefono, celular from empresas WHERE status = 1;";
                conn.Open();
                cmd.ExecuteNonQuery();
                MySqlDataAdapter dm = new MySqlDataAdapter(cmd);
                DataTable ls = new DataTable();
                dm.Fill(ls);

                grid.DataSource = ls;

            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("El nombre no existe");

            }

        }

        /*Boton con la funcion de de insertar datos en la base de datos*/
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            String nombre    = tfNom.Text;
            String razon     = tfRaz.Text;
            String direccion = tfDir.Text;
            String telefono  = tfTel.Text;
            String celular   = tfCel.Text;

            Conexion strConexion = new Conexion();
            MySqlConnection conn = strConexion.conexion();

            MySqlCommand cmd = conn.CreateCommand();


            try
            {
                if (nombre.Length == 0 || razon.Length == 0 || direccion.Length == 0 || telefono.Length .Equals("(   )   - ") || celular.Equals("   -   -"))
                {
                    MessageBox.Show("No puede dejar ningun campo vacio");
                }
                else
                {
                    cmd.CommandText = "INSERT INTO empresas value ('" + nombre + "', '" + razon + "', '" + direccion + "', '" + telefono + "', '" + celular + "', 1)";
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Guardado Correctamente");
                    limpiarFormulario();
                    llernarGrid(dataGridView1);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("El nombre ya existe");
            }
        }      
        /*Boton con la funcion de actulizar datos en la bd*/
        private void btnActualizar_Click(object sender, EventArgs e)
        {
            String nombre    = tfNom.Text;
            String razon     = tfRaz.Text;
            String direccion = tfDir.Text;
            String telefono  = tfTel.Text;
            String celular   = tfCel.Text;

            Conexion strConexion = new Conexion();
            MySqlConnection conn = strConexion.conexion();

            MySqlCommand cmd = conn.CreateCommand();
            try
            {
                if (nombre.Length == 0 || razon.Length == 0 || direccion.Length == 0 || telefono.Length.Equals("(   )   - ") || celular.Equals("   -   -"))
                {
                    MessageBox.Show("No puede dejar ningun campo vacio");
                }
                else
                {

                    cmd.CommandText = "UPDATE empresas SET nombre='" + nombre + "', razon_social='" + razon + "', direccion='" + direccion + "', telefono='" + telefono + "', celular='" + celular + "' WHERE nombre='" + nombre + "'";
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Actualizado Correctamente");
                    limpiarFormulario();
                    btnActualizar.Enabled = false;
                    btnEliminar.Enabled = false;
                    tfNom.Enabled = true;
                    btnAgregar.Enabled = true;
                    llernarGrid(dataGridView1);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("El nombre no existe");
              
            }
        }
        /*Boton que contiene la funcion de eliminar*/
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            String nombre = tfNom.Text;

            Conexion strConexion = new Conexion();
            MySqlConnection conn = strConexion.conexion();

            MySqlCommand cmd = conn.CreateCommand();
            try
            {
                cmd.CommandText = "UPDATE empresas SET status = 0 WHERE nombre='" + nombre + "'";
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Eliminado Correctamente");
                limpiarFormulario();
                btnActualizar.Enabled = false;
                btnEliminar.Enabled = false;
                btnAgregar.Enabled = true;
                tfNom.Enabled = true;
                llernarGrid(dataGridView1);
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("El nombre no existe");

            }
        }
        /*Boton que contiene la funcion de buscar una empresa atravez de su id nombre*/
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Conexion strConexion = new Conexion();
            MySqlConnection conn = strConexion.conexion();
            MySqlDataReader reader = null;
            MySqlCommand cmd = conn.CreateCommand();

            String nombre = tfNom.Text;

            try
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.CommandText = "SELECT razon_social, direccion, telefono, celular FROM empresas WHERE nombre='" + nombre + "'";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        tfRaz.Text = reader.GetString(0);
                        tfDir.Text = reader.GetString(1);
                        tfTel.Text = reader.GetString(2);
                        tfCel.Text = reader.GetString(3);
                    }
                    btnActualizar.Enabled = true;
                    btnEliminar.Enabled   = true;
                    btnAgregar.Enabled    = false;
                    tfNom.Enabled         = false;
                }
                else
                {
                    limpiarFormulario();
                    MessageBox.Show("No existe esa empresa");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show(ex.ToString());
            }
        }
        /*Metodo que inicializa objetos al mismo tiempo que se corre la aplicacion*/
        private void Form1_Load(object sender, EventArgs e)
        {
            llernarGrid(dataGridView1);
            btnActualizar.Enabled = false;
            btnEliminar.Enabled   = false;
        }
        /*Boton que cierra la actual y abre el menu*/
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm = new Form4();
            frm.Show();

        }
    }    
}

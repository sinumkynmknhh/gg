﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    class Conexion
    {
        public MySqlConnection conexion()
        {
            String servidor = "localhost";
            String nombreBase = "mudanza";
            String usuario = "root";
            String password = "admin";
            String cadenaConexion = "";

            try
            {
                 cadenaConexion = "Data Source=" + servidor + ";Initial Catalog=" + nombreBase + ";Persist Security Info=True;User ID=" + usuario + ";Password=" + password + "";

                MySqlConnection conexion = new MySqlConnection();
                conexion.ConnectionString = cadenaConexion;
                return conexion;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al conectar al servidor de MySQL: " + ex.Message, "Error al conectar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

        }
    }
}
